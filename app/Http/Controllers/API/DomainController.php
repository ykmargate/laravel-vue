<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Models\Domain;

class DomainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() : Response
    {
        // CODE CHALLENGE: Search Filter
        $domains = Domain::all();
        // Create HTTP response, 200 ok.

        // Return HTTP response.
        return new Response($domains, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) : Response
    {
        // CODE CHALLENGE: Add Domains
        $d = new Domain();
        $domains = request('domains');
        $list = explode("\n", $domains);
        foreach ($list as $element){
            $domain_name = trim($element);
            if($domain_name != ''){
                try {
                $d->create(['domain_name'=>trim($element)]);
                    $response = new Response(['status' => ['code' => 0, 'message' => 'Domains added successfully!']], Response::HTTP_OK);
                }
                catch (\Exception $e){
                    $response = new Response(['status' => ['code' => 1, 'message' => 'Something went wrong!']], Response::HTTP_OK);
                }
            }
        }
        // Return HTTP response.
        return $response;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) : Response
    {
        // CODE CHALLENGE: Toggle Imprint
        //dd($request);
        try {
            $d = new Domain();
            $rec = $d->find($id);
            $rec->is_imprinted = request('is_imprinted');
            $rec->is_idn = request('is_idn');
            $rec->save();
            $response = new Response(['status' => ['code' => 0, 'message' => 'Domain updated successfully!']], Response::HTTP_OK);
        }
        catch (\Exception $e){
            $response = new Response(['status' => ['code' => 1, 'message' => 'Something went wrong!']], Response::HTTP_OK);
        }
        // Return HTTP response.
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id) : Response
    {
        // CODE CHALLENGE: Delete Domains
        try {
            Domain::destroy($id);
            $response = new Response(['status' => ['code' => 0, 'message' => 'Domain deleted successfully!']], Response::HTTP_OK);
        }
        catch (\Exception $e){
            $response = new Response(['status' => ['code' => 1, 'message' => 'Something went wrong!']], Response::HTTP_OK);
        }
        // Return HTTP response.
        return $response;
    }
}
